// index.js
import express from 'express';
import path from 'path';
import cors from 'cors';
import router from './public/js/api';
import db from './public/js/db';
import session from 'express-session';

const app = express();
const Port = process.env.PORT || "8000";
app.use(cors());

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
app.use(express.static(path.join(__dirname, "public")));

app.use(session({
  secret: 'squirly whirl',
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 3600000 }
}))

app.use(router);

app.listen(Port, () => {
  console.log('Listening to requests on http://localhost:' + Port);
});
