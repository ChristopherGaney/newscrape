import db from '../db';
import mysql from 'mysql';
import api from '../models/api-model';
import parser from '../models/api-parser';

let all;
//let error = 0;
let insertKeyword = (art_id,word) => {
	let query1 = "INSERT IGNORE INTO keywords (word) VALUES ?;";
	let query2 = "SELECT id FROM keywords WHERE word = ?";
	let query3 = "INSERT INTO articlekeys (keywordId,articleId) VALUES ?;";
	let arr = [[word]];
	let key_id = '';

	// Last operation: set keyword -- article relations
	let setArticleKeys = (key_id) => {
		arr = [[key_id,art_id]]
		db.query(query3, [arr], (err, result) => {
           if (err) {
           	  console.log('error inserting articlekey');
              console.log(err);
           } 
        });
	};

	// Insert keyword
	db.query(query1, [arr], (err, result) => {
				key_id = result.insertId;
		           if (err) {
		             console.log('error inserting keyword');
		             console.log(err);
		           }
		           if(key_id) {
		           	//if not ignored, we'll have a key
		           	setArticleKeys(key_id);
		           }
		           else {
		           	// Keyword was a duplicate, so we get keyword id
		           	db.query(query2, [arr], (err, result) => {
		           	   key_id = result[0].id;
			           if (err) {
			              console.log('error getting keyword id');
			              console.log(err);
			           } 
			           if(key_id) {
				           setArticleKeys(key_id);
				       }
			        });

		           }
	        });
};
let processKeywords = (id,word) => {
	let i,len,list;
	if(word) {
		list = word.split(',');
		len = list.length;
		for(i = 0;i < len;i++) {
			
			if(list[i].length > 0) {
				insertKeyword(id,list[i].trim());
			}
		}
	}
	else {
		
	}
};
let insertBulkArticles = (values) => {
	 let query = "INSERT INTO articles (parent,src,loc,title,changed) VALUES ?";
         
        db.query(query, [values], (err, result) => {
           if (err) {
              console.log('error in insertBulkArticles');
              console.log(err);
           } 
          console.log('list-builder.js finished inserting bulk articles');
        });
};
let insertSingleArticle = (arr,word) => {
	let query = "INSERT IGNORE INTO articles (parent,src,loc,title,changed) VALUES ?";
	
		db.query(query, [arr], (err, result) => {
				if (err) {
			              console.log('error inserting single article');
			              console.log(err);
			           }
				if(result.insertId) {
					let id = result.insertId;
			        processKeywords(id,word); 
			    }
			    else {
			    	return;
			    }
		     });
	
};
let hasWordsOrNot = (res,p,g) => {
	let i,d,word,arr;
	let len = res.length;
	let values = [];
	
	
	if(res[0].word === '') {
		console.log('list-builder.js inserting ' + len + ' bulk articles.');
		for(i = 0;i < len;i++) {
			d = res[i].changed.substring(0, res[i].changed.length - 1);
			arr = [p,g,res[i].loc,res[i].title,d];
			values.push(arr);
		}
		insertBulkArticles(values);
	}
	else {
		console.log('list-builder.js inserting ' + len + ' single articles.');
		for(i = 0;i < len;i++) {
			d = res[i].changed.substring(0, res[i].changed.length - 1);
			arr = [[p,g,res[i].loc,res[i].title,d]];
			word = res[i].word;
			insertSingleArticle(arr,word);
			if(i === (len - 1)) {
				console.log('list-builder.js finished inserting ' + len + ' single articles');
			}
		}
	}
};



let mediaLooper = (cb1,cb2) => {
	
    		let len = all.length;
    		let i = 0, j = 0;
    		let urls,lan;
   			let d = new Date();
   			let t = d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();

    		for(i = 0;i < len;i++) {
    			urls = all[i]['urls'];
    			lan = urls.length;
    			
    			for(j = 0;j < lan;j++) {
    				cb2(urls[j]);
    			}
    			if(i === (len - 1)) {
    				cb1('parsing urls for ' + len + ' media sources');
    			}
    		}
    	};

module.exports = {
    parseAll: (cb) => {

    	let query = "ALTER TABLE articlekeys DROP FOREIGN KEY key_id, DROP FOREIGN KEY art_id;TRUNCATE keywords;TRUNCATE articlekeys;TRUNCATE articles;ALTER TABLE articlekeys ADD CONSTRAINT key_id FOREIGN KEY (keywordId) REFERENCES keywords(id);ALTER TABLE articlekeys ADD CONSTRAINT art_id FOREIGN KEY (articleId) REFERENCES articles(id)";
         // Truncate Tables
        db.query(query, (err, result) => {
        	let d = new Date();
   			let t = d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
           if (err) {
              console.log(err);
           } 
           console.log('list-builder.js ' + t + ' Tables Truncated');
           api.getMedias((result) => {
	        all = result;
	      
	      	// Parse each Media's Urls
	        mediaLooper(cb,function(url) {
	        		let len,i;
    				let p = url.id;
    				let g = url.parent;
    				
    				if(url.method === 'flat-xml') {
	    				parser.getFlat(url, (result) => {
			    			hasWordsOrNot(result,p,g);
				        });
				    }
				    else {
				    	parser.getDeep(url, (result) => {
			    			len = result.length;
			    			
							for(i = 0;i < len;i++) {
								if(result[i].length > 0) {
									hasWordsOrNot(result[i],p,g);
								}
							}
				        });
				    }
				});
	      });
          
        });  
      }
 };