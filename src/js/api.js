import express from 'express';
import bodyParser from 'body-parser';
import multer from 'multer';
import api from './models/api-model';
import parser from './models/api-parser';
import scraper from './models/api-scraper';
import user from './models/user';
import builder from './builders/list-builder';

const router = express.Router();
const upload = multer();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
  extended: true
}));

router.use(function (req, res, next) {
  var user =  req.session.user,
  userId = req.session.userId;
  console.log(req.path);
  /*if(req.path !== '/' && req.path !== '/signup' && req.path !== '/signup/' &&
    req.path !== '/logout' && req.path !== '/logout/' && req.path !== '/login' && req.path !== '/login/') {
    console.log('passed?');
    if(userId == null){
      res.redirect('/');
     return;
    }
  }*/
  next()
})

router.get('/', (req, res) => {
  let success = '';
  let mess = req.session['success'];
  if(mess && mess.length) {
    success = mess;
  }
  res.render('signin', { message: success });
});

router.get('/signup', (req, res) => {
  res.render('signup');
});

router.get('/logout', (req, res) => {
  req.session.destroy((err) => {
      res.redirect('/');
   })
});

router.get('/dashboard', (req, res) => {
  res.render('index', { message: '' });
});

router.get('/api', upload.none(), (req, res) => {
    console.log('api.js getMedias()');
      api.getMedias((result) => {
        console.log('api.js returning getMedias result to client\n');
        res.json(result);
      });
       
});
router.put('/api', upload.none(), (req, res) => {
   const item = req.body;
   if(item.req === 'add-cp') {
      console.log('api.js addMedia()');
      api.addMedia(item, (result) => {
        console.log('api.js returning addMedia() result to client\n');
        res.json(result);
      });
    }
    else if(item.req === 'add-url') {
      console.log('api.js addUrl()');
      api.addUrl(item, (result) => {
        console.log('api.js returning addUrl() result to client\n');
        res.json(result);
      });
    }  
});
router.delete('/api', upload.none(), (req, res) => {
   const query = req.query;
   if(query.req === 'del-cp') {
      console.log('api.js removeMedia()');
      api.removeMedia(query, (result) => {
        console.log('api.js returning removeMedia() result to client\n');
        res.json(result);
      });
    }
    else if(query.req === 'del-url') {
        console.log('api.js removeUrl()');
        api.removeUrl(query, (result) => {
          console.log('api.js returning removeUrl() result to client\n');
          res.json(result);
        });
    } 
});
router.patch('/api', upload.none(), (req, res) => {
   const item = req.body;
      console.log('api.js updateUrl()');
      api.updateUrl(item.params, (result) => {
        console.log('api.js returning updateUrl() result to client\n');
        res.json(result);
      });
       
});
router.post('/poster', upload.none(), (req, res) => {
   const item = req.body;
   console.log(item);
   if(item.type === 'xml') {
     if(item.method === 'raw-xml') {
      console.log('api.js getRaw()');
        parser.getRaw(item.url, (result) => {
            console.log('api.js returning getRaw() result to client\n');
            res.json(result);
          });
      }
      else if(item.method === 'flat-xml') {
        console.log('api.js getFlat()');
        parser.getFlat(item, (result) => {
            console.log('api.js returning getFlat() result to client\n');
            res.json(result);
          });
      }
      else if(item.method === 'deep-xml') {
        console.log('api.js getDeep()');
        parser.getDeep(item, (result) => {
            console.log('api.js returning getDeep() result to client\n');
            res.json(result);
          });
      }
    }
    else {
      if(item.method === 'raw-http') {
        console.log('api.js getRawHttp()');
        scraper.getRawHttp(item.url, (result) => {
          console.log('api.js returning getRawHttp() result to client\n');
          res.json(result);
        });
      }
      else if(item.method === 'flat-http') {
        console.log('api.js getFlatHttp()');
        scraper.getFlatHttp(item.url, (result) => {
          console.log('api.js returning getFlatHttp() result to client\n');
          res.json(result);
        });
      }
      else if(item.method === 'schema-http') {
        console.log('api.js getFlatHttp()');
        scraper.getSchemaHttp(item.url, (result) => {
          console.log('api.js returning getFlatHttp() result to client\n');
          res.json(result);
        });
      }
      else if(item.method === 'deep-http') {
        console.log('api.js getDeepHttp()');
        scraper.getDeepHttp(item.url, (result) => {
          console.log('api.js returning getDeepHttp() result to client\n');
          res.json(result);
        });
      }
    }
});
router.post('/api', upload.none(), (req, res) => {
   const item = req.body;
   let d = new Date();
   let t = d.getTime();
   if(item.action === 'parse-all') {
      console.log('api.js parseAll()');
      builder.parseAll((result) => {
          console.log('api.js returning parseAll() result to client\n');
          res.json(result);
      });
   }
   if(item.action === 'logout') {
      console.log('api.js parseAll()');
      builder.parseAll((result) => {
          console.log('api.js returning parseAll() result to client\n');
          res.json(result);
      });
   }
});
router.post('/signup', upload.none(), (req, res) => {
   user.signUp(req, (result) => {
    console.log('in callback');
      if(result === 'success') {
        req.session['success'] = 'Congrats! You Are Signed Up!';
        res.redirect('/');
    }
      else {
        res.render('signup', { message: 'An Error occurred. Please try again.' });
      }
   });
});
router.post('/login', upload.none(), (req, res) => {
  
   user.login(req, (result) => {
      console.log('in callback');
      if(result === 'success') {
        res.redirect('/dashboard');
      }
      else {
        res.render('signin', { message: 'An Error occurred. Please try again.' });
      }
   });
});
module.exports = router;