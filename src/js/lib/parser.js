import ajax from './ajax.js';
import viewer from './viewer.js';
import objectCache from './objectCache.js';
import subpub from './subPub.js';

let parser = (() => {
        let params;
        let updated = false;

         let spinner = (() => {
            let spin = $('.spinner');
            return {
                showSpinner: () => {
                    spin.show('fast', function() {
                        spin.addClass('progress');
                    });
                    
                },
                hideSpinner: () => {
                    spin.removeClass('progress');
                    spin.hide('fast');
                }
            };
        })();
        
        let setParseListener = () => {
            //console.log('setting');
            let lastone = () => {
                let stack = objectCache.getit('xmlStack');
                viewer.displayXML(stack.result, stack.params, parseURLItemHTML.setButton,setParseListener);
            };
           $('#get_words').off('click').on('click', lastone); 
        };

       let makeRequest = (params) => {
            spinner.showSpinner();
            ajax.sendRequest('/poster', params, (response) => {
                let res = response.data;
                spinner.hideSpinner();

                if(params.type === 'xml') {
                    viewer.displayXML(res, params, parseURLItemHTML.setButton,setParseListener);
                    objectCache.pushit({"stack": "xmlStack", "result": res, "params": params});
                }
                else {
                    viewer.displayHTML(res, params);
                }
            });
        };
        var parseMediasRequest = (params) => {
            spinner.showSpinner();
            ajax.sendRequest('/api', params, (response) => {
                let res = response.data;
                let display = $('#display_tb');
                spinner.hideSpinner();
                if(res.code == 500) {
                    display.html("message: " + res.message + "<br>code: " + res.code);
                }
                else {
                    display.text("Node says: status Ok");
                }
            });
        };
        var makeListRequest = () => {
            let params = "?list=bigList";
            
            spinner.showSpinner();
            
            if(!updated) {
                ajax.getRequest('/api' + params, (response) => {
                    let res = response.data;
                    spinner.hideSpinner();
                   viewer.displayList(res, editListItem.setButton, parseListItem.setButton, editMediaItem.setButton);
                   objectCache.pushit({"stack": "listStack", "result": res});
                });    
                updated = true;
                //console.log('updated');
            }
            else {
                let stack = objectCache.getit('listStack');
                spinner.hideSpinner();
                viewer.displayList(stack.result, editListItem.setButton, parseListItem.setButton, editMediaItem.setButton);
                //console.log(stack);
                //console.log('got archive');
            }
        };
        
        var addItemRequest = (params) => {
            spinner.showSpinner();
            ajax.addRequest('/api', params, (response) => {
                let display = $('#display_tb');
                let res = response.data;
                spinner.hideSpinner();
                if(res.code == 500) {
                    display.html("message: " + res.message + "<br>code: " + res.code);
                }
                else {
                    display.text("Node says: status Ok");
                }
            });
        };
        var deleteItemRequest = (params) => {
            spinner.showSpinner();
            ajax.deleteRequest('/api', params, (response) => {
                let display = $('#display_tb');
                let res = response.data;
                spinner.hideSpinner();
                if(res.code == 500) {
                    display.html("message: " + res.message + "<br>code: " + res.code);
                }
                else {
                    display.text("Node says: status Ok");
                }
            });
        };
        var modifyItemRequest = (params) => {
            spinner.showSpinner();
            ajax.modifyRequest('/api', params, (response) => {
                let display = $('#display_tb');
                let res = response.data;
                spinner.hideSpinner();
                if(res.code == 500) {
                    display.html("message: " + res.message + "<br>code: " + res.code);
                }
                else {
                    display.text("Node says: status Ok");
                }
            });
        };
        var setParserSidebar = (type) => {
        	if(type === 'xml') {
                $('#http-method-type').fadeOut('slow', () => {
	            	$('#http-string-type').fadeOut('slow');
	                $('#xml-method-type').fadeIn('slow');
	                $('#xml-string-type').fadeIn('slow');
            	});
            }
            else {
                $('#xml-method-type').fadeOut('slow', () => {
                	$('#xml-string-type').fadeOut('slow');
                	$('#http-method-type').fadeIn('slow');
                    $('#http-string-type').fadeIn('slow');
                }); 
            }
        };
        
         // Handler for Sidebar Parser Inputs
        var handleRadios = (() => {
            let type = '';
             let isSet = false;
            $('#resource-method').submit((e) => {
                let type = $('input[name=rsource_typ]:checked').val();
                e.preventDefault();
                e.stopImmediatePropagation();

                if(type === 'xml') {
                    let url = '', method = '', tname = '', top = '', loc = '', 
                        title = '', changed = '', word = '', hour = '', lang = '', pop = '';
                        url = $('#inp_url').val();
                        //type = $('input[name=rsource_typ]:checked').val();
                        method = $('input[name=rsource_met]:checked').val();
                        top = $('#obj_top').val();
                        loc = $('#obj_loc').val();
                        title = $('#obj_title').val();
                        changed = $('#obj_changed').val();
                        word = $('#obj_word').val();
                        hour = $('#obj_hour').val();
                        lang = $('#obj_lang').val();
                        pop = $('#obj_pop').val();
                        let obj = {
                            "url": url,
                            "type": type,
                            "method": method,
                            "top": top,
                            "loc": loc,
                            "title": title,
                            "changed": changed,
                            "word": word,
                            "hour": hour,
                            "lang": lang,
                            "pop": pop
                          };
                        if(url !== '' && type !== '' && method !== '' && top != '' && loc != '') {
                            makeRequest(obj);
                        }
                        else {
                            alert('Please select all fields.');
                            console.log('required fields empty!');
                        }
               }
               else {
                    let url = '', method = '';
                    url = $('#inp_url').val();
                    //type = $('input[name=rsource_typ]:checked').val();
                    method = $('input[name=http_typ]:checked').val();
                    let obj = {
                       "url": url,
                        "type": type,
                        "method": method 
                    };
                    if(url !== '' && type !== '' && method !== '') {
                        //console.log('method');
                        //console.log(method);
                        makeRequest(obj);
                    }
                    else {
                        alert('Please select all fields.');
                        console.log('required fields empty!');
                    }
               }
            });
            $('input[name=rsource_typ]').on('click', () => {
                let id = '';
                type = $('input[name=rsource_typ]:checked').val();
               	setParserSidebar(type); 
            }); 
       })();

       // Get the List of Content Providers
       let getList = (() => {
            $('#show-list').on('click', () => {
               makeListRequest(); 
            });
       })();
       let parseAll = (() => {
            $('#parse_all').on('click', () => {
                params = { "action": "parse-all"};
                parseMediasRequest(params);
            });
       })();

      // Handler for Editor Modal Inputs
       $('#new-item-form').submit((e) => {
                let name = '', url = '', type = '', method = '', id = '', urlid = '', req = '',
                top = '', loc = '', title = '', changed = '', word = '', hour = '24', lang = '', pop = '';
                updated = false;
                name = $('#i_name').val();
                url = $('#i_url').val();
                type = $('#i_type').val();
                method = $('#i_method').val();
                id = $('#i_mediaid').val();
                urlid = $('#i_urlid').val();
                top = $('#i_top').val();
                loc = $('#i_loc').val();
                title = $('#i_title').val();
                changed = $('#i_changed').val();
                word = $('#i_word').val();
                hour = $('#i_hour').val();
                lang = $('#i_lang').val();
                pop = $('#i_pop').val();
                req = $('input[name=protor]:checked').val();
                let obj = {"req": req,
                        "name": name,
                        "url": url, 
                        "type": type, 
                        "method": method,
                        "id": id,
                        "urlid": urlid,
                        "top": top,
                        "loc": loc,
                        "title": title,
                        "changed": changed,
                        "word": word,
                        "hour": hour,
                        "lang": lang,
                        "pop": pop
                        };
                e.preventDefault();
                e.stopImmediatePropagation();
                $('#save_display').off('click', viewer.setSave);
                
                if(req === 'add-cp') {
                    if(name !== '' && url !== '' && type !== '' && method !== '') {
                        $.modal.close();
                        addItemRequest(obj);
                    }
                    else if(name !== '') {
                        $.modal.close();
                        addItemRequest({"req": req,
                                    "name": name});
                    }
                }

                else if(req === 'del-cp' && id != '') {
                    $.modal.close();
                    deleteItemRequest({"req": req,
                                    "id": id});
                }
                else if(req === 'add-url' && id !== '' && url !== '' && type !== '' && 
                    method !== '' && top !== '' && loc !== '') {
                    $.modal.close();
                    addItemRequest(obj);
                }
                 else if(req === 'del-url' && urlid !== '') {
                    $.modal.close();
                    deleteItemRequest({"req": req,
                                    "urlId": urlid});
                }
                else if(req === 'modify' && urlid !== '' && url !== '' && type !== '' && 
                    method !== '' && top !== '' && loc !== '') {
                    $.modal.close();
                    modifyItemRequest(obj);
                }
                else {
                    alert('Please select all necessary fields.');
                    console.log('required fields empty!');
                }
            });


       let editParserItem = (() => {
            $('.add-btn a[data-modal]').click((event) => {
                let name = '', url = '', type = '', method = '', tname, itemid = '', urlid = '', top = '',
                 loc = '', title = '', changed = '', word = '', hour = '', lang = '', pop = '';
                
                name = $('#hidden_name').val();
                url = $('#inp_url').val();
                type = $('input[name=rsource_typ]:checked').val();
                itemid = $('#hidden_id').val();
                urlid = $('#hidden_urlid').val();
                //console.log('clicked');
                if(type === 'xml') {
                    tname = 'xml_typ';
                }
                else {
                    tname = 'http_typ';
                }
                
                method = $('input[name=rsource_met]:checked').val().trim();
                top = $('#obj_top').val();
                loc = $('#obj_loc').val();
                title = $('#obj_title').val();
                changed = $('#obj_changed').val();
                word = $('#obj_word').val();
                hour = $('#obj_hour').val();
                lang = $('#obj_lang').val();
                pop = $('#obj_pop').val();
                if(name !== '') {
                    
                    $('#i_name').val(name);
                }
                $('#i_url').val(url);
                $('#i_type').val(type);
                $('#i_method').val(method);
                $('#i_mediaid').val(itemid);
                $('#i_urlid').val(urlid);
                $('#i_top').val(top);
                $('#i_loc').val(loc);
                $('#i_title').val(title);
                $('#i_changed').val(changed);
                $('#i_word').val(word);
                $('#i_hour').val(hour);
                $('#i_lang').val(lang);
                $('#i_pop').val(pop);
              $('#ex1').modal();
              return false;
            });
       })();
       let editMediaItem = (() => {
            let setEditButton = () => {
                let wasClicked = (event) => {
                    let name = '', parent = '';
                   let sub = $(event.target).closest('.name_row');
                    parent = sub.find('.mediaid').text();
                    name = sub.find('.listname').text();
                    
                    $('#i_name').val(name);
                    $('#i_mediaid').val(parent);

                  $('#ex1').modal();
                  return false;
                };
                $('.ers a.media_edit').off('click').on('click', wasClicked);
        };

        return {
            setButton: () => {
                setEditButton();
            }

        };
       })();
       let editListItem = (() => {
            let setEditButton = () => {
                let wasClicked = (event) => {
                    let name = '', url = '', type = '', method = '', parent = '', id = '',
                    top = '', loc = '', title = '', changed = '', word = '', hour = '', lang = '', pop = '';
                   let sub = $(event.target).closest('.url_wrapper_wrap').find('.urlid');
                    parent = sub.data('pid');
                    id = sub.data('id');
                    let stack = objectCache.getit('listStack');
                    //console.log(stack);
                    //console.log('top edit buttonnnn');
                    $.each(stack.result, (i,v) => {
                        if(v.media.id === parent) {
                            name = v.media.name;
                            $.each(v.urls, (j,w) => {
                                if(w.id === id) {
                                    type = w.resource;
                                    method = w.method;
                                    url = w.url;
                                    top = w.top;
                                    loc = w.loc;
                                    title = w.title;
                                    changed = w.changed;
                                    word = w.word;
                                    hour = w.hour;
                                    lang = w.lang;
                                    pop = w.pop;
                                }
                            });
                        }
                    });
                     
                    $('#i_name').val(name);
                    $('#i_url').val(url);
                    $('#i_type').val(type);
                    $('#i_method').val(method);
                    $('#i_mediaid').val(parent);
                    $('#i_urlid').val(id);
                    $('#i_top').val(top);
                    $('#i_loc').val(loc);
                    $('#i_title').val(title);
                    $('#i_changed').val(changed);
                    $('#i_word').val(word);
                    $('#i_hour').val(hour);
                    $('#i_lang').val(lang);
                    $('#i_pop').val(pop);

                  $('#ex1').modal();
                  return false;
                };
                $('.ers a.url_edit').off('click').on('click', wasClicked);
        };

        return {
            setButton: () => {
                setEditButton();
            }

        };
       })();


       let parseListItem = (() => {
            let setParseButton = () => {
                let wasClicked = (event) => {
                    let name = '', url = '', type = '', method = '', ptype = '', pmethod = '', parent = '',
                    id = '', top = '', loc = '', title = '', changed = '', word = '', hour = '', lang = '', pop = '';
                    
                    let sub = $(event.target).closest('.url_wrapper_wrap').find('.urlid');
                    parent = sub.data('pid');
                    id = sub.data('id');
                    let stack = objectCache.getit('listStack');
                    //console.log(stack);
                    $.each(stack.result, (i,v) => {
                        if(v.media.id === parent) {
                            name = v.media.name;
                            $.each(v.urls, (j,w) => {
                                if(w.id === id) {
                                    type = w.resource;
                                    method = w.method;
                                    url = w.url;
                                    top = w.top;
                                    loc = w.loc;
                                    title = w.title;
                                    changed = w.changed;
                                    word = w.word;
                                    hour = w.hour;
                                    lang = w.lang;
                                    pop = w.pop;
                                }
                            });
                        }
                    });
                  
                    $('#hidden_id').val(parent);
                    $('#hidden_urlid').val(id);
                    $('#hidden_name').val(name);
                    $('#inp_url').val(url);
                    
                   if(type !== "" && type === "xml") {
                        ptype = "xml_typ";
                        if(method !== '' && method === "deep-xml") {
                            pmethod = "deep-xml";
                        } else if(method !== '' && method === "flat-xml") {
                            pmethod = "flat-xml";
                        } else {
                            pmethod = "raw-xml";
                        }
                    }
                    if(type !== "" && type === "http") {
                        ptype = "http_typ";
                        if(method !== '' && method === "deep-http") {

                            pmethod = "deep_http";
                        } else if(method !== '' && method === "flat-http") {
                            pmethod = "flat_http";
                        } else {
                            pmethod = "raw_http";
                        }
                    }

                    setParserSidebar(type);

                    $('#' + ptype).prop("checked", true);
                    $('#' + pmethod).prop("checked", true);
                    $('#obj_top').val(top);
                    $('#obj_loc').val(loc);
                    $('#obj_title').val(title);
                    $('#obj_changed').val(changed);
                    $('#obj_word').val(word);
                    $('#obj_hour').val(hour);
                    $('#obj_lang').val(lang);
                    $('#obj_pop').val(pop);

                };
                
                $('.prs a.item_parse').off('click').on('click', wasClicked);
        };

        return {
            setButton: () => {
                setParseButton();
            }
        };
       })();
       let parseURLItemHTML = (() => {
            let setParseURLButton = () => {
                let wasClicked = (event) => {
                    let box = $(event.target);
                    let url = box.data('parse');
                    let type = 'http';
                    let pmethod = 'schema_http';

                    $('#inp_url').val(url);
                    $('#http_typ').prop("checked", true);
                    $('#' + pmethod).prop("checked", true);
                    setParserSidebar(type);
                };
                
                $('.url_parse_html a.url_item_parse').off('click').on('click', wasClicked);
        };
        return {
            setButton: () => {
                setParseURLButton();
            }
        };
       })();
    
})();
module.exports = parser;
