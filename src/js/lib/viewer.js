import ajax from './ajax.js';

let viewer = (() => {

    function stripScripts(s) {
        let div = document.createElement('div');
        div.innerHTML = s;
        let scripts = div.getElementsByTagName('script');
        let i = scripts.length;
        while (i--) {
          scripts[i].parentNode.removeChild(scripts[i]);
        }
        return div.innerHTML;
      }

       return {
        
        displayXML: (result, params, cb1, cb2) => {
            let display = $('#display_tb');
            let items = '';
            let check = 0;
            let counter = $('#list_count');
            let count = result.length;
            //console.log('logging');
            //console.log(result);
            if(params.method === "deep-xml") {
               items += `<div class="list_wrapper">`;

                $.each(result, (i,sub) => {
                    count += sub.length;

                    $.each(sub, (i,v) => {
                        let m = '',t = '',w = '';
                        if(v.title) {
                            t = v.title;
                        }
                        else {
                            t = v.loc;
                        }
                        if(v.changed) {
                            m = v.changed;
                        }
                        if(v.word) {
                            w = v.word;
                        }
                
                        items += `<div class="list_tables">
                                    <div class="row name_row">
                                        <div class="twelve columns"><a href="` + v.loc + `" target="_blank">` + t + `</a></div>
                                        <div class="six columns url_date">` + m + `</div>
                                        <div class="six columns url_parse_html"><a href="javascript:void(0)"` +
                                        ` class="btn url_item_parse" name="url-item-parse" data-parse="` + v.loc + `">Parse</a></div>
                                        <div class="twelve columns">` + w + `</div>
                                    </div>
                                  </div>`;
                    });
                });

                items += `</div>`;
            }
            else if(params.method === "flat-xml") {
                items += `<div class="list_wrapper">`;
                    
                    $.each(result, (i,v) => {
                        let m = '',t = '',w = '';
                        if(v.title) {
                            t = v.title;
                        }
                        else {
                            t = v.loc;
                        }
                        if(v.changed) {
                            m = v.changed;
                        }
                        if(v.word) {
                            w = v.word;
                        }
                
                        items += `<div class="list_tables">
                                    <div class="row name_row">
                                        <div class="twelve columns"><a href="` + v.loc + `" target="_blank">` + t + `</a></div>
                                        <div class="six columns url_date">` + m + `</div>
                                        <div class="six columns url_parse_html"><a href="javascript:void(0)"` +
                                        ` class="btn url_item_parse" name="url-item-parse" data-parse="` + v.loc + `">Parse</a></div>
                                        <div class="twelve columns">` + w + `</div>
                                    </div>
                                  </div>`;
                    });

                items += `</div>`;
            }
            
            else if(params.method === "raw-xml") {
            
               items += '<textarea id="save_text" style="width: 100%; min-height: 500px;">' + result + '</textarea>';
                check = 1;
            }
            display.html(items);
            counter.val(count);

            /*$('#fancytable').DataTable({
                "searching": true
            });*/
            cb1();
            cb2();
           /* if(check === 1) {
                $('#save_display').on('click', viewer.setSave);
                $('#clear_display').on('click', viewer.setClear);
            }
            else {
                $('#save_display').off('click', viewer.setSave);
                $('#clear_display').off('click', viewer.setClear);
            }
            $('#delete_display').off('click').on('click', viewer.setDelete);*/
        },

        displayHTML: (result,params) => {
            let display = $('#display_tb');
            let items = '';
            
            //let check = 0;
            
            if(params.method === 'schema-http' || params.method === 'flat-http') {
                let v = result;
                let article = '';

                if(v.article && v.article.length > 0) {
                    if(v.article.length > 550) {
                        article = v.article.substring(0,550) + '. . .';
                    }
                    else {
                        article = v.article;
                    }
                }

                items += `<div class="list_wrapper">
                            <div class="list_tables">
                                <div class="row name_row">
                                    <div class="twelve columns"><a href="` + v.loc + `" target="_blank">` + v.title + `</a></div>
                                    <div class="six columns art_heading">Author</div>
                                    <div class="six columns art_heading">LastMod</div>
                                    <div class="six columns author">` + v.author + `</div>
                                    <div class="six columns lastmod">` + v.changed + `</div>
                                    <div class="twelve columns full_heading">Image Url</div>
                                    <div class="twelve columns"><a href="` + v.imgurl + `" target="_blank">` + v.imgurl + `</a></div>
                                    <div class="twelve columns full_heading">Description</div>
                                    <div class="twelve columns art_desc">` + v.description + `</div>
                                    <div class="twelve columns full_heading">Keywords</div>
                                    <div class="twelve columns art_word">` + v.word + `</div>
                                    <div class="twelve columns full_heading">Article</div>
                                    <div class="twelve columns article">` + article + `</div>
                                </div>
                            </div>
                        </div>`;
                display.html(items);
            }
            else {
                items = result;
                display.text(items);
            }
            //items += '<textarea id="save_text" style="width: 100%; min-height: 500px;"><pre><code>' + /*stripScripts(result)*/result + '</code></pre></textarea>';
            //check = 1;
            
        },
        displayList: (result, cb1, cb2, cb3) => {
            let display = $('#display_tb');
            let items = '';
            //console.log(result);
            $('#save_display').off('click', viewer.setSave);
            items += '<div class="list_wrapper">';
               
               $.each(result, (i,v) => {
                    let len = v.length;
                    let count = 0;

                   items += `<div class="list_tables">
                                <div class="row name_row">
                                     <div class="one columns mediaid">` + v.media.id + `</div>
                                    <div class="seven columns listname">` + v.media.name + `</div>
                                    <div class="four columns">
                                            <span class="ers">
                                                <a href="#ex1" class="btn top_edit item_edit media_edit" data-modal>Edit</a>
                                            </span>
                                        </div>
                                </div>`;

                    $.each(v.urls, (i,m) => {
                        let cls = '';
                        if(len > 1) {
                            if(count % 2 === 0) { 
                                cls = ' even';
                            } else { 
                                cls = ' odd';
                            }
                         } 

                       items += `<div class="url_wrapper_wrap"><div class="row url_row` + cls + `">
                                        <div data-id="` + m.id + `" data-pid="` + m.parent + `" class="one columns urlid">` + m.id + `</div>
                                        <div class="eleven columns">
                                            <a href="` + m.url + `" target="_blank" class="listurl">` + m.url + `</a>
                                        </div>
                                    </div>
                                    <div class="row meta_row">
                                        <div class="one columns"></div>
                                        <div class="seven columns">
                                            <span class="listtype">` + m.resource + `</span>
                                            <span class="listmethod">` + m.method + `</span>
                                        </div>
                                        <div class="four columns">
                                            <span class="prs">
                                                <a href="javascript:void(0)" class="btn item_parse" name="item-parse" value="parse">Parse</a>
                                            </span>
                                            <span class="ers">
                                                <a href="#ex1" class="btn item_edit url_edit" data-modal>Edit</a>
                                            </span>
                                        </div>
                                    </div>
                                    
                                </div>`;
                        });
                        items += `</div>`;
                });
                items += '</div>';
                display.html(items);
                cb1();
                cb2();
                cb3();
                //$('#delete_display').off('click').on('click', viewer.setDelete);
        }

    };

})();
module.exports = viewer;
