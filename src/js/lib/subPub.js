let pubsub = (() => {

  const subscribers = {}

  function publish(eventName, data) {
    if (!Array.isArray(subscribers[eventName])) {
      return;
    }
    subscribers[eventName].forEach((callback) => {
      callback(data);
    })
  }

  function subscribe(eventName, callback) {
	  if (!Array.isArray(subscribers[eventName])) {
	    subscribers[eventName] = []
	  }
	  subscribers[eventName].push(callback)
	  const index = subscribers[eventName].length - 1

	  return {
	    unsubscribe() {
	    	console.log('called unsscirbee');
	      subscribers[eventName].splice(index, 1)
	    },
	  }
	}

  return {
    publish,
    subscribe
  };

})();
module.exports = pubsub;

/*

https://jsmanifest.com/the-publish-subscribe-pattern-in-javascript/

const ps = subpub;
const unsub = ps.subscribe('show-money', showMeTheMoney);
ps.publish('show-money', ' yeah dog&#$')

unsubscribe event with:
unsub.unsubscribe();

*/