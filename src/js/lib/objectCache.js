let objectCache = (() => {
    let stack = {
        xmlStack: {},
        listStack: {}
    };

    return {
        pushit: (obj) => {
            stack[obj.stack]  = obj;
           // console.log('pushing');
           // console.log(stack[obj.stack]);
        },
        getit: (stk) => {
            //console.log('lastone');
            if(stack[stk].result) {
                return stack[stk];
            }
        }
    };
})();
module.exports = objectCache;