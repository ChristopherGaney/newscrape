import axios from 'axios';

let ajax = (() => {

    return {
        getRequest: (ext, cb) => {
            axios.get(ext)
              .then(cb)
              .catch((error) => {
                console.log(error);
              });
        },

        sendRequest: (ext, t, cb) => {
            axios.post(ext, t)
              .then(cb)
              .catch((error) => {
                console.log(error);
              });
        },

        addRequest: (ext, t, cb) => {
            axios.put(ext, t)
              .then(cb)
              .catch((error) => {
                console.log(error);
              });
        },

        deleteRequest: (ext, t, cb) => {
            axios.delete(ext, {params:t})
              .then(cb)
              .catch((error) => {
                console.log(error);
              });
        },
        modifyRequest: (ext, t, cb) => {
            axios.patch(ext, {params:t})
              .then(cb)
              .catch((error) => {
                console.log(error);
              });
        }

       };
})();
module.exports = ajax;
