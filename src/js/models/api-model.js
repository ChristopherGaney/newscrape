import db from '../db';
import mysql from 'mysql';

module.exports = {
    getMedias: (cb) => {
      let query_1 = "SELECT * FROM medias ORDER BY id ASC";
      let query_2 = "SELECT * FROM medialist WHERE parent = ?";
      let urls = [];
      let data = [];
      let media = {};
      let i,len = 0;

      let resulter = function(i,id) {
          db.query(query_2, id, (err, result) => {
            if (err) {
              console.log(err);
            }
            urls.push({"media": media[i], "urls": result});
            if(i === (len-1)) {
              console.log('api-model.js returning ' + urls.length + ' items from getMedias()');
             cb(urls);           
            }            
          });
        };
        db.query(query_1, (err, res) => {
          len = res.length;
          media = res;
          if (err) {
            console.log(err);
          }
          for(i = 0;i < len;i++) {
            resulter(i,res[i].id);
          } 
        });
    },
      addMedia: (data, cb) => {
         let query_1 = "INSERT INTO medias (name) VALUES ?";
         let values_1 = [
                  [data.name]
                ];
         let query_2 = "INSERT INTO medialist (url,resource,method,parent,top,loc,title,changed,word,hour,lang,pop) VALUES ?";
         let values_2;
               db.query(query_1, [values_1], (err, result) => {
                    if (err) {
                          console.log(err);
                    }
                    if(data.url) {
                      values_2 = [
                          [data.url, data.type, data.method, result.insertId, data.top, data.loc,
                          data.title, data.changed, data.word, data.hour, data.lang, data.pop]
                        ];
                      db.query(query_2, [values_2], (err, result) => {
                            if (err) {
                                  console.log(err);
                            }
                          console.log('api-model.js api-model.js result from addMedia()');
                          console.log(result);
                          cb(result);
                      });
                    }
                    else {
                        console.log('api-model.js api-model.js result from addMedia()');
                        console.log(result);
                        cb(result);
                    }
              });
     },
     addUrl: (data, cb) => {
         let query = "INSERT INTO medialist (url,resource,method,parent,top,loc,title,changed,word,hour,lang,pop) VALUES ?";
         let values = [
                  [data.url, data.type, data.method, data.id, data.top, data.loc, data.title, data.changed, data.word, data.hour,data.lang,data.pop]
                ];
               db.query(query, [values], (err, result) => {
                    if (err) {
                          console.log(err);
                  } 
                  console.log('api-model.js result from addUrl()');
                  console.log(result);
                  cb(result);
              });
     },
     removeMedia: (data, cb) => {
       let query = "DELETE FROM medias WHERE id = ?";
       let values = data.id;
            db.query(query, values, (err, result) => {
                  if (err) {
                        console.log(err);
                  }
                  console.log('api-model.js result from removeMedia()');
                  console.log(result);
                cb(result);
            });
     },
     removeUrl: (data, cb) => {
       let query = "DELETE FROM medialist WHERE id = ?";
       let values = data.urlId;
            db.query(query, values, (err, result) => {
                  if (err) {
                        console.log(err);
                  }
                  console.log('api-model.js result from removeUrl()');
                  console.log(result);
                cb(result);
            });
     },
     updateUrl: (data, cb) => {
      let query = "UPDATE medialist SET resource = ?, method = ?, top = ?, loc = ?, title = ?, changed = ?, word = ?, hour = ?, lang = ?, pop = ? WHERE id = ?";
       let values = [data.type, data.method, data.top, data.loc, data.title, data.changed, data.word, data.hour, data.lang, data.pop, data.urlid];
             db.query(query, values, (err, result) => {
                  if (err) {
                        console.log(err);
                  }
                 console.log('api-model.js result from updateUrl()');
                  console.log(result); 
                cb(result);
            });
     }
};
