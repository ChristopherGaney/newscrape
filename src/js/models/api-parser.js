import db from '../db';
import {parseString} from 'xml2js';
import xpath from 'xml2js-xpath';
import ht from 'http-https';

let count = 0;

let parseXmlObject = (obj, par, cb) => {
   let len = obj.length;
   let arr = [];
   let ret = [];
   let filterDateTime = (par.hour) ? (par.hour * 60 * 60 * 1000) : 86400000;// default = 24 hours
   let i = 0;
   
   arr.push(par.top.trim().split(','));
   arr.push(par.loc.trim().split(','));
   arr.push(par.title.trim().split(','));
   arr.push(par.changed.trim().split(','));
   arr.push(par.word.trim().split(','));
   arr.push(par.lang.trim().split(','));
   
   let unPacker = function(item) {
      let x,y;
      let lc = '', tl = '', md = '', wd = '', lg = '';

      let objector = function(arr) {
        let l = arr.length;

         if(l === 1) {
            return item[arr[0]];
          }
          else if(l === 2) {
            
            if(item[arr[0]][0][arr[1]]) {
              return item[arr[0]][0][arr[1]];
            }
            else {
              return '';
            }
          }
          else if(l === 3) {
            return item[arr[0]][0][arr[1]][0][arr[2]];
          }
          else {
            return '';
          }
       };
     
      if(arr[1][0] !== '') {
          lc = objector(arr[1]);
          lc = lc[0];
       }
     if(arr[2][0] !== '') {
          tl = objector(arr[2]);
          tl = tl[0];
       }
      if(arr[3][0] !== '') {
          md = objector(arr[3]);
          md = md[0];
       }
      if(arr[4][0] !== '') {
        wd = objector(arr[4]);
        wd = wd[0];
      }
      if(arr[5][0] !== '') {
        lg = objector(arr[5]);
        lg = lg[0];
      } 
      // filtering by date here
      if(md !== '') {
         x = new Date().getTime() - filterDateTime;
         y = new Date(md);
        if(y < x) {
          return;
        }
      }
      // filtering by language
      if(lg !== '') {
        if(lg !== 'en') {
          return;
        }
      }       
      
      ret.push({"loc": lc, "title": tl, "changed": md, "word": wd});
      count++;
   };
  
     for(i = 0;i < len;i++) {
      unPacker(obj[i]);
      if(i === (len-1)) {
        cb(ret);
      }
     }
  };

module.exports = {
    getRaw: (url, cb) => {
          let req = ht.get(url, function(res) {
              let data = '';
              res.on('data', function(stream) {
                  data += stream;
              });
              res.on('end', function(){
                  console.log('api-parser.js returning ' + data.length + ' items from getRaw()');
                  cb(data);
              });
          });

      },
    getFlat: (par, cb) => {
        
          let req = ht.get(par.url, function(res) {
            let data = '';
            res.on('data', function(stream) {
                data += stream;
            });
            res.on('end', function(){
              parseString(data, function(error, result) {
                  let matches = '';
                    if(error === null) {
                      matches = xpath.find(result, '//' + par.top);
                      parseXmlObject(matches, par, function(res) {
                        console.log('api-parser.js returning ' + count + ' items from getFlat()');
                        count = 0;
                        cb(res);
                      }); 
                    }
                    else {
                        console.log(error);
                    }
                });
            });
        });

      },
      getDeep: (par, cb) => {
          let arr = [], bar = [];
          let len = 0;
          let lan = 0;
          let isSet = false;
          let timerie = function() {
            let repeater = function() {
              if(bar.length === lan) {
                console.log('api-parser.js returning ' + count + ' items from getDeep()');
                count = 0;
                cb(bar);
              }
              else {
                setTimeout(function() {
                  timerie();
                },400);
              }
            };
            if(isSet === false) {
              isSet = true;
              repeater();
            } 
          };
          let finisher = function() {
            lan = arr.length;
            let j = 0;
          
            for(j = 0;j < lan;j++) {
              parseXmlObject(arr[j], par, function(res) {
                 bar.push(res);
                 if(j === (lan-1)) {
                  timerie();
                 }
              });
            }
          };
          let resulter = function(i,sitemap) {
            let req = ht.get(sitemap, function(res) {
            let data = '';
            res.on('data', function(stream) {
                data += stream;
            });
            res.on('end', function(){
              parseString(data, function(error, result) {
                let matches = '';
                let intvl;
                if(error === null) {
                  matches = xpath.find(result, '//' + par.top);
                 
                  if(matches.length > 0) {
                      arr.push(matches); 
                  }
                  // checking for pop parameter
                  if(par.pop && par.pop !== 'null') {
                    finisher(); 
                    clearInterval(intvl);
                  }
                  else if(i === (len-1)) {
                    intvl = setInterval(function() {
                       if(arr.length === (len-1)) {
                        finisher(); 
                        clearInterval(intvl);
                       }  
                   },100);   
                  }  
                }
                else {
                  console.log(error);
                }
              });
            });
          });
          };
          let req = ht.get(par.url, function(res) {
            let data = '';
            res.on('data', function(stream) {
                data += stream;
            });
            res.on('end', function(){
              parseString(data, function(error, result) {
                  let matches = '', popped, i = 0;

                    if(error === null) {
                      matches = xpath.find(result, "//loc");
                      len = matches.length;
                      // checking for pop parameter
                      if(par.pop && par.pop !== 'null') {
                        console.log('passed');
                        if(par.pop === 'first') {
                          popped = matches[0];
                        }
                        else {
                          popped = matches[len - 1];
                        }
                        resulter(i,popped);
                      }
                      else {
                        for(i = 0;i < len;i++) {
                            resulter(i,matches[i]);
                        }
                      }
                    }
                    else {
                        console.log(error);
                    }
                });
            });
        });
      }
};
