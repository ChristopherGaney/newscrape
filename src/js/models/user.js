import db from '../db';
import mysql from 'mysql';
import bcrypt from 'bcrypt';

const saltRounds = 10;

module.exports = {
    
     login: (req, cb) => {
         
         let sess = req.session; 
          console.log('in login');
         if(req.method == "POST"){
            let post  = req.body;
            let name= post.user_name;
            let pass= post.password;
        
            let sql = "SELECT id, first_name, last_name, user_name, password FROM users WHERE user_name ='"+name+"'";                           
            db.query(sql, (err, results) => { 
              if (err) {
                console.log(err);
              }     
               if(results.length){
                  bcrypt.compare(pass, results[0].password, function (err, result) {
                    if(result) {
                      req.session.userId = results[0].id;
                      req.session.user = results[0];
                      cb('success');
                    }
                    else {
                      cb('fail');
                    }
                  });
               }
               else{
                  cb('fail');
               }
                       
            });
            
         } else {
            cb('fail');
         }        
      },
      signUp: (req, cb) => {
         
         if(req.method == "POST"){
            let post = req.body;
            let name = post.user_name;
            let pass = post.password;
            let fname = post.first_name;
            let lname = post.last_name;
       
            
       
            bcrypt.hash(pass, saltRounds, function (err,   hash) {
              let sql = "INSERT INTO users(first_name,last_name,user_name, password) VALUES ('" +
              fname + "','" + lname + "','" + name + "','" + hash + "')";
              let query = db.query(sql, (err, result) => {
                if (err) {
                  console.log(err);
                }
                 cb('success');
              });
            });
       
         } else {
            cb('fail');
         }
      }
};