import db from '../db';
//import {parseString} from 'xml2js';
//import xpath from 'xml2js-xpath';
import ht from 'http-https';
import cheerio from 'cheerio';

module.exports = {
	getRawHttp: (url, cb) => {
          //var opt = url.parse(url)
           /* ht.headers = {
              'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.3 (KHTML, like Gecko) Version/8.0 Mobile/12A4345d Safari/600.1.4'
            }*/
          let req = ht.get(url, function(res) {
              let data = '';
              res.on('data', function(stream) {
                  data += stream;
              });
              res.on('end', function(){
                  console.log('api-scraper.js returning ' + data.length + ' items from getHttp()');
                  cb(data);
              });
          });

      },
      getSchemaHttp: (url, cb) => {
        let req = ht.get(url, function(res) {
            let data = '';
            res.on('data', function(stream) {
                data += stream;
            });
            res.on('end', function(){
                const $ = cheerio.load(data);
                let title = '', changed = '', author = '', description = '',
                word = '', article = '', imgurl = ''; 

                title = $('title').text();
                
                /*$('meta').each(function(i, el) {
                    let name = $(el).attr('name');

                    if(name) {
                      if(name === 'lastmod') {
                        changed = $(el).attr('content');
                      }
                      else if(name === 'author') {
                        author = $(el).attr('content');
                      }
                      else if(name === 'description') {
                        description = $(el).attr('content');
                      }
                      else if(name === 'keywords') {
                        word = $(el).attr('content');
                      }
                    }
                });*/
                /*$('meta').each(function(i, el) {
                    let name = $(el).attr('name');

                    if(name) {
                      if(name === 'last_updated_date') {
                        changed = $(el).attr('content');
                      }
                      /*else if(name === 'author') {
                        author = $(el).attr('content');
                      }
                      else if(name === 'description') {
                        description = $(el).attr('content');
                      }
                      else if(name === 'keywords') {
                        word = $(el).attr('content');
                      }
                    }
              });*/
              //let dats = JSON.parse($('[data-qa=schema]').html());
              let dats = JSON.parse($('script[type="application/ld+json"]').html());
              changed = dats.dateModified;
              author = dats.author.name;
              description = dats.description; 
              imgurl = dats.image.url;
              article = $('div.article-body').html();

              cb({"loc": url, "title": title, "changed": changed, "author": author,
                 "description": description, "word": word, "article": article, "imgurl": imgurl});
            });
        });

      },
      getFlatHttp: (url, cb) => {
        let req = ht.get(url, function(res) {
            let data = '';
            res.on('data', function(stream) {
                data += stream;
            });
            res.on('end', function(){
                const $ = cheerio.load(data);
                let title = '', changed = '', author = '', description = '',
                word = '', article = '', imgurl = ''; 

                title = $('title').text();
                
                $('meta').each(function(i, el) {
                    let name = $(el).attr('name');

                    if(name) {
                      if(name === 'lastmod') {
                        changed = $(el).attr('content');
                      }
                      else if(name === 'author') {
                        author = $(el).attr('content');
                      }
                      else if(name === 'description') {
                        description = $(el).attr('content');
                      }
                      else if(name === 'thumbnail') {
                        console.log('yes thumbnail');
                        imgurl = $(el).attr('content');
                      }
                      else if(name === 'keywords') {
                        word = $(el).attr('content');
                      }
                    }
                });
                /*$('meta').each(function(i, el) {
                    let name = $(el).attr('name');

                    if(name) {
                      if(name === 'last_updated_date') {
                        changed = $(el).attr('content');
                      }
                      /*else if(name === 'author') {
                        author = $(el).attr('content');
                      }
                      else if(name === 'description') {
                        description = $(el).attr('content');
                      }
                      else if(name === 'keywords') {
                        word = $(el).attr('content');
                      }
                    }
              });*/
              //let dats = JSON.parse($('script[type="application/ld+json"]').html());
              //console.log(typeof dats)
              //console.dir(dats);
              //changed = dats.dateModified;
              //author = dats.author.name;
              //description = dats.description; 
              //imgurl = dats.image.url;
              article = $("div[itemprop = 'articleBody']").html();

              cb({"loc": url, "title": title, "changed": changed, "author": author,
                 "description": description, "word": word, "article": article, "imgurl": imgurl});
            });
        });

      }
};

/*
walk = ({ tagName, attribs, children = [] }) => ({
  tagName,
  attribs,
  children: children.map(walk),
}); 
JSON.stringify(walk($('html')[0]), null , 2);
*/