const mysql = require('mysql');

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'scrape',
  password: 'More',
  database: 'scraper',
  socketPath: '/var/run/mysqld/mysqld.sock',
  multipleStatements: true
});

connection.connect((err) => {
  if (err) {
  	console.log('Error connecting to DB');
  	return;
  }
  console.log('Connected to MYSQL!\n');
});

module.exports = connection;