"use strict";

var _express = _interopRequireDefault(require("express"));

var _path = _interopRequireDefault(require("path"));

var _cors = _interopRequireDefault(require("cors"));

var _api = _interopRequireDefault(require("./public/js/api"));

var _db = _interopRequireDefault(require("./public/js/db"));

var _expressSession = _interopRequireDefault(require("express-session"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// index.js
var app = (0, _express["default"])();
var Port = process.env.PORT || "8000";
app.use((0, _cors["default"])());
app.set("views", _path["default"].join(__dirname, "views"));
app.set("view engine", "pug");
app.use(_express["default"]["static"](_path["default"].join(__dirname, "public")));
app.use((0, _expressSession["default"])({
  secret: 'squirly whirl',
  resave: false,
  saveUninitialized: true,
  cookie: {
    maxAge: 3600000
  }
}));
app.use(_api["default"]);
app.listen(Port, function () {
  console.log('Listening to requests on http://localhost:' + Port);
});
