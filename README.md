## XMLParser

I needed to parse XML sitemaps for an application that I was building, so I built a dashboard to examine and parse flat and nested sitemaps. Sitemaps can be edited and saved with their meta-data, and a 'ParseAll' feature will loop through all of the sitemaps storing all the URLs and related keywords into a MySQL database. The next phase in building will be visiting all of the URLs to collect any data such as title, keywords, or image URL that wasn't contained in the XML sitemap.

I first built this application while I was learning Go ([https://github.com/ChristopherGaney/xmlViewer](https://github.com/ChristopherGaney/xmlViewer)). More recently, I rebuilt the server-side code using Node.js, Express, and xml2js library.




